# Todo #

## User ##
* edit + edit view
* delete + delete button
* list for admin
* show for admin
* everything depending on user group 

## groups ##
* Table in db
* col1 => id
* col2 => label

prefil: 1 for member, 2 for writers, 3 for admin

## articles ##
* DB
* CRUD
* Ctrl
* Views
* Model
* admin + writer rights
* just about everything

## other ##
* dispatcher
* objectify router
* repair sess class
* htaccess
