<?php


	require_once('Views/Layouts/layout.php');

	function call($controller, $action) {
	// require the file that matches the controller name
	require_once('Controllers/' . $controller . '_controller.php');
	// create a new instance of the needed controller
	switch($controller) {
		case 'app':
		$controller = new AppController();
		break;
		case 'posts':
		require_once("Models/Post.php");
		$controller = new PostsController();
		break;
		case 'user':
		require_once("Models/User.php");
		$controller = new UsersController();
		break;
	}


	// call the action
	$controller->{ $action }();
	}

	// array key=>val ex: controller => user, action => show, id => 1
	function redirect($arr = null) {
		$str = "/php-rush-mvc//";
		if (isset($arr)) {
			$str = $str . "?";
			foreach($arr as $key => $val) {
				$str = $str . $key . "=" . $val . "&";
			}
			$str = substr($str, 0, -1);
		}
		exit(header("Location: ". $str));
		ob_end_flush();
	}

	// just a list of the controllers we have and their actions
	// we consider those "allowed" values
	$controllers = array('app' => ['index', 'layout', 'error'],
						'posts' => ['index', 'show', 'create', 'edit', 'delete'],
						'user' => ['index', 'show', 'create', 'logout']);

	// check that the requested controller and action are both allowed
	// if someone tries to access something else he will be redirected to the error action of the pages controller
	if (array_key_exists($controller, $controllers)) {
		if (in_array($action, $controllers[$controller])) {
		call($controller, $action);
		} else {
		call('app', 'error');
		}
	} else {
	call('app', 'error');
	}
?>