<?php 
require_once("i_controller.php");
class AppController implements IController{

	public function index() {
		$first_name = 'Jon';
		$last_name  = 'Snow';
		require_once("Views/home.php");
	}

	public function layout() {
		require_once("Views/Layouts/layout.php");
	}

	public function error() {
		require_once('Views/error.php');
	}

	public function create() {
		call("app", "error");
	}
	public function show() {
		call("app", "error");
	}
	public function edit() {
		call("app", "error");
	}
	public function delete() {
		call("app", "error");
	}

}

?>