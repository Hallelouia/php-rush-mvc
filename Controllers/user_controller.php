<?php
require_once("i_controller.php");

class UsersController implements IController {

	public function index() {
		if ($_SERVER["REQUEST_METHOD"] == 'GET')
			if (!isset($_SESSION['user']))
				require_once("Views/User/login.php");
			else
				return call('user', 'show');
		else if ($_SERVER["REQUEST_METHOD"] == 'POST') {
			$res = User::login($_POST["email"], $_POST["pass"]);
			if ($res == false) {
				require_once("Views/fail.php");
			} else {
				$_SESSION['user'] = $res->array_serialize();
				redirect(["controller" => "user", "action" => "show", "id" => $_SESSION['user']['id']]);
//				exit(header("Location: /php-rush-mvc/?controller=user&action=show&id=". $_SESSION['user']['id'])); // do not use call we need to redirect
//				ob_end_flush();
			}
		} else {
			return call('app', 'error');
		}
	}

	public static function logout() {
		$_SESSION['user'] = null;
		exit(header("Location: /php-rush-mvc/?controller=app&action=index")); // do not use call we need to redirect
		ob_end_flush(); // the ob wil be restarted at the beginning of the layout
	}

	public function create() {
		if ($_SERVER["REQUEST_METHOD"] == "GET") {
			require_once ("Views/User/create.php");			
		}
		else if ($_SERVER["REQUEST_METHOD"] == "POST") {
		
			if (empty($_POST["uname"]) || empty($_POST["email"]) || empty($_POST["pass"]) || empty($_POST["passv"]) && $_POST["pass"] != $_POST["passv"])
				return call('app', 'error');
			 $success = User::create($_POST["uname"] ,$_POST["email"], password_hash($_POST["pass"], PASSWORD_BCRYPT));
			if ($success) {
				require_once('Views/success.php');
				return;
			}
			else {
				require_once('Views/fail.php');
				return ;
			}
		} else {
			return call('app', 'error');
		}
	}
	// List function goes there (adm only)
	
	// should use find by id even for current user loop back
	public function show() {
	// show user, 
	// we expect a url of form ?controller=user&action=show&id=x
	// without an id we just redirect to the error page as we need the post id to find it in the database
	if (!Session::get('user'))
		return call('user', 'index');
		// we use the given id to get the right post
		// check session for admin rights
		$user = User::find($_SESSION['user']['id']);
		require_once('Views/User/show.php');
		}

	public function edit() {
		if ($_SERVER["REQUEST_METHOD"] == "GET") {
			require_once ("Views/User/create");			
		}
		else if ($_SERVER["REQUEST_METHOD"] == "POST") {
		if (!isset($_POST['data'])/* Do all the checks !!!*/)
			return call('app', 'error');
		$success = User::edit(/* Params go there*/);
		if ($success)
			require_once('Views/success.php');
		else
			require_once('Views/fail.php');
		} else 
			return call('app', 'error');
		// we use the given id to get the right user
		// check session for admin rights or
		// user to edit is logged user 
	}

	public function delete() {
		if (!isset($_GET['id']))
			return call('app', 'error');
		// we use the given id to get the right post
		// check session for admin rights
		// $_GET
		$success = User::delete($_GET['id']);
		if ($success)
			require_once('Views/success.php');
		else
			require_once('Views/fail.php');
	}
	}
?>