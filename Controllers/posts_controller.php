<?php
require_once("i_controller.php");

class PostsController implements IController {

	public function create() {
		if ($_SERVER["REQUEST_METHOD"] == "GET") {
			require_once ("Views/Posts/create.php");
		}
		else if ($_SERVER["REQUEST_METHOD"] == "POST") {
		
			if (empty($_POST["cont"]) || empty($_POST["auth"]))
				return call('app', 'error');
		
			 $success = Post::add($_POST["auth"] ,$_POST["cont"]);

			if ($success) {
				exit(header("Location: /php-rush-mvc/?controller=posts&action=index")); // make a function for this shit
				return call('posts', 'index');
			}
			else {
				require_once('Views/fail.php');
				return ;
			}
			return call('app', 'error');
		}
	}

	public function edit() {
		if ($_SERVER["REQUEST_METHOD"] == "GET") {
			require_once ("Views/Posts/edit.php");
		}
		else if ($_SERVER["REQUEST_METHOD"] == "POST") {

			if (empty($_POST["cont"]) || empty($_POST["auth"]))
				return call('app', 'error');
			$success = Post::edit($_POST["auth"] ,$_POST["cont"], $_GET["id"]);
			if ($success) {
				exit(header("Location: /php-rush-mvc/?controller=posts&action=index")); // make a function for this shit
				return call('posts', 'index');
			}
			else {
				require_once('Views/fail.php');
				return ;
			}
			return call('app', 'error');
		}
	}

	public function delete() {
		if (!isset($_GET["id"]))
			return call('app', 'error');
			$success = Post::delete($_GET["id"]);
			if ($success) {
				exit(header("Location: /php-rush-mvc/?controller=posts&action=index")); // make a function for this shit
				return call('posts', 'index');
			}
			else {
				require_once('Views/fail.php');
				return ;
			}
			return call('app', 'error');
	}

	public function index() {
	// we store all the posts in a variable
		$posts = Post::all();
		require_once('Views/Posts/index.php');
	}

	public function show() {
		// we expect a url of form ?controller=posts&action=show&id=x
		// without an id we just redirect to the error page as we need the post id to find it in the database
		if (!isset($_GET['id']))
		return call('app', 'error');

		// we use the given id to get the right post
		$post = Post::find($_GET['id']);
		require_once('Views/Posts/show.php');
		}
	}
?>