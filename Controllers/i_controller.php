<?php
interface IController {
	public function index();
	public function create();
	public function show();
	public function edit();
	public function delete();

}
?>