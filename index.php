<?php
	require_once('connection.php');
	include_once('Session.php');
	session_start();
    if (isset($_GET['controller']) && isset($_GET['action'])) {
    $controller = $_GET['controller'];
    $action     = $_GET['action'];
  } else if  (!isset($_GET['controller']) && !isset($_GET['action'])){
    $controller = 'app';
    $action     = 'home';
  } else {
    $controller = 'app';
    $action     = 'error';
	}

	require_once('routes.php');

//	require_once('Views/Layouts/layout.php');
?>