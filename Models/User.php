<?php 
class User {

	CONST ADMIN = 1;
	CONST WRITER = 2;
	CONST MEMBER = 3;
	// Mand fields
	public $_userName; // Name
	public $_userEmail; // Email
	// Non Mandatory fields
	public $_userId; // ID
	public $_userGroupId; // GID
	public $_userPass; // password, will always be hashed with password_hash(string, PASSWORD_BCRYPT)
	public $_userCreatedAt; // join date 
	public $_userMod; // last update
	public $_userStatus; // false when not banned

	// password will be prehashed by ctrl
	function __construct($ue, $un, $us, $id, $gid, $pass, $c_at, $lu) {
		$this->_userName = $un;
		$this->_userGroupId = $gid;
		$this->_userEmail = $ue;
		$this->_userId = $id;
		$this->_userStatus = $us;
		$this->_userMod = $lu;
		$this->_userCreatedAt = $c_at;
		$this->_userPassword = $pass; // used only on create
	}

	public static function create($name, $email, $pass) {
		$db = Db::getInstance();
		$req = $db->prepare("INSERT INTO users (u_name, email, password, group_id, status, created_at, last_modified) VALUES (:un, :e, :pass, :gid, 0, NOW(), NOW())");
		$res = $req->execute(array(":e" => $email, ":un" => $name,
		":pass" => $pass, ":gid" => self::MEMBER));
		return $res;
	}

	public static function login($email, $pass) {
		$db = Db::getInstance();
		$req = $db->prepare("SELECT * from users where email=:e");
		$req->execute(array(":e" => $email));
		$res = $req->fetch();
		if (password_verify($pass, $res["password"])) {
			return new User($res["email"], $res["u_name"], $res["status"], $res["id"], $res["group_id"], $res["password"], $res["created_at"], $res["last_modified"]);
		}
		return false;
	}

	public static function all() {
		$db = Db::getInstance();
		$req = $$db->prepare("SELECT * from users");
		$req->execute();
		foreach ($req->fetchAll() as $user) {
			yield new User($user["email"], $user["u_name"], $user["status"], $user["id"], $user["group_id"], $user["password"], $user["created_at"], $user["last_modified"]);
		}
	}

	public static function find($id) {
		$db = Db::getInstance();
		$req = $db->prepare("SELECT * from users where id=:e");
		$req->execute(array(":e" => $id));
		$res = $req->fetch();
		return new User($res["email"], $res["u_name"], $res["status"], $res["id"], $res["group_id"], $res["password"], $res["created_at"], $res["last_modified"]);
	}

	public static function edit($user) {
		$db = Db::getInstance();
		$pass = password_hash($pass, PASSWORD_BCRYPT);

		$req = $db->prepare("UPDATE users SET group_id = :gid, email = :e, status = :st, password = :p, last_modified = NOW() WHERE id = :id");
		$req->execute(array(":e" => $user["Email"], ":us" => $user["status"],
		":pass" => $user["pass"], ":gid" => $user["gid"], ":id" => $user["id"]));
		return $req->rowCount() == 1;
	}

	public static function remove($id) {
		$db = Db::getInstance();
		$req = $db->prepare(" delete from users where id=:e");
		$req->execute(array(":e" => $id));
		return $req->rowCount == 1;
	}

	// to return the object as an array so it can be saved in the session
	public function array_serialize() {
		return [
			'id' => $this->_userId,
			'email' => $this->_userEmail,
			'name' => $this->_userName,
			'password' => $this->_userPass,
			'gid' => $this->_userGroupId,
			'created_at' => $this->_userCreatedAt,
			'last_modified' => $this->_userMod,
			'status' => $this->_userStatus
		];
	}

	// Create a user from an array
	public function array_deserialize($user_array) {
		return new User($user_array["email"], $user_array["name"], $user_array["status"], $user_array["id"], $user_array["gid"] ,$user_array["password"], $user_array["created_at"], $user_array["last_modified"]);
	}

	
}
?>